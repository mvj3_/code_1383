package com.android.antking.xml;

import java.io.OutputStream;
import java.util.List;

import org.xmlpull.v1.XmlSerializer;

import android.util.Xml;


/**采用pull 生成xml文件
 * 
 * @author antkingwei
 *
 */
public class PullBuildXMLService {

	public void buildXML(List<Person> persons,OutputStream outputStream)throws Exception{
		XmlSerializer serializer = Xml.newSerializer();
		serializer.setOutput(outputStream,"utf-8");
		serializer.startDocument("utf-8", true);
		serializer.startTag(null, "perisons");
		for(Person person:persons){
			serializer.startTag(null, "perison");
			serializer.attribute(null, "id",String.valueOf(person.id));
			
			serializer.startTag(null, "name");
			serializer.text(person.name);
			serializer.endTag(null, "name");
			
			serializer.startTag(null, "age");
			serializer.text(String.valueOf(person.age));
			serializer.endTag(null, "age");

			serializer.endTag(null, "perison");
		}
		serializer.endTag(null, "perisons");
		serializer.endDocument();
		outputStream.close();
	}
}
